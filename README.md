# Dashboard

## Requirements

- Nginx 1.6+
- Python 2.7

## Git or FTP

Copy files to your server, Path: `/home/wwwroot/dashboard/dashboard.xxx.com`, or change it to your favorite.

Then execute the following command:

```bash
chown -R www:www /home/wwwroot/dashboard/dashboard.xxx.com
```

Make sure the directory is writable(dashboard.xxx.com).

## Configure

#### Nginx

File: `/usr/local/nginx/conf/vhost/dashboard.conf`

```
server
    {
        listen 80;
        server_name dashboard.xxx.com;
        index index.html;
        root  /home/wwwroot/dashboard/dashboard.xxx.com;

        location ~ .*\.(js|css)?$
        {
            expires      12h;
        }

        location ~ /\.git
        {
            deny all;
        }

        location ~ /\.
        {
            deny all;
        }

        access_log  /home/wwwlogs/dashboard.xxx.com.log;
    }
```

`server_name`, `root` and `access_log`, please set a valid value according to the actual situation.

#### Crontab

Add record to file `/etc/crontab`:

```bash
*/1 * * * * www /usr/bin/python /home/wwwroot/dashboard/dashboard.xxx.com/cron.py > /home/wwwroot/dashboard/dashboard.xxx.com/table.json
```

#### cron.py

Line 13:

```
DB_PATH = '/path/to/your/datadb'
```

#### Run

Reload your nginx and open your browser to check it.

```bash
nginx -s reload
```

:)