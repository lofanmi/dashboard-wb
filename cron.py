#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import sys
reload(sys)
sys.setdefaultencoding('utf8')

import time
import datetime
import os
import json

DB_PATH = './datadb'

if not os.path.exists(DB_PATH):
    print 'DB_PATH is invalid.'
    sys.exit(1)

user_number    = 0
record_count   = 0
sum_using_time = {}
sum_step       = {}

for item in os.listdir(DB_PATH):
    path = DB_PATH + '/' + item
    if not os.path.isdir(path):
        continue

    user_number += 1

    for t in os.listdir(path):
        key = int(t)
        p = path + '/' + t + '/data.json'

        with open(p, 'r') as f:
            data = json.load(f)
        if not data:
            continue

        key = t

        # { "steps": 22, "loginTime": "000001", "logoutTime": "000010" }
        for item in data['Log']:
            record_count   += 1

            if not sum_using_time.has_key(key):
                sum_using_time[key] = 0
            if not sum_step.has_key(key):
                sum_step[key] = 0

            sum_using_time[key] += int(item['logoutTime']) - int(item['loginTime'])
            sum_step[key]       += item['steps']

data = {
    'user_number'    : user_number,
    'record_count'   : record_count,
    'sum_using_time' : sum_using_time,
    'sum_step'       : sum_step,
}

print json.dumps(data, sort_keys=True, indent=4)
